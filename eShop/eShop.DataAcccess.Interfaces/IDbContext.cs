﻿using eShop.Domain;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace eShop.DataAcccess.Interfaces
{
    public interface IDbContext
    {
        DbSet<Product> Products { get; }

        Task<int> CommitAsync(CancellationToken cancellationToken);
    }
}
