﻿using AutoMapper;
using eShop.DataAcccess.Interfaces;
using eShop.Usecases.Interfaces.Exceptions;
using eShop.Usecases.Interfaces.Products.Dto;
using eShop.Usecases.Interfaces.Products.Queries.GetById;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace eShop.Usecases.Products.Queries.GetProductById
{
    internal class GetProductByIdQueryHandler : IRequestHandler<GetProductByIdQuery, ProductDetailDto>
    {
        private readonly IDbContext _context;
        private readonly IMapper _mapper;

        public GetProductByIdQueryHandler(IDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProductDetailDto> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            var product = await _context.Products.FindAsync(new object[] { request.ProductId }, cancellationToken);

            if (product == null)
            {
                throw new ObjectNotFoundException();
            }

            return _mapper.Map<ProductDetailDto>(product);
        }
    }
}
