﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using eShop.DataAcccess.Interfaces;
using eShop.Usecases.Interfaces.Products.Queries.SearchProducts;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace eShop.Usecases.Products.SearchProducts
{
    internal class SearchProductsHandler : IRequestHandler<SearchProductsQuery, IReadOnlyList<ProductDto>>
    {
        private readonly IDbContext _context;
        private readonly IMapper _mapper;

        public SearchProductsHandler(IDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IReadOnlyList<ProductDto>> Handle(SearchProductsQuery request, CancellationToken cancellationToken)
        {
            var query = _context.Products
                            .AsNoTracking()
                            .AsQueryable();

            var filter = request.Filter;
            if (!string.IsNullOrEmpty(filter.Name))
                query = query.Where(x => x.Name.StartsWith(filter.Name));

            if (filter.CategoryId.HasValue)
                query = query.Where(x => x.CategoryId == filter.CategoryId.Value);

            if (filter.PriceFrom.HasValue)
                query = query.Where(x => x.Price >= filter.PriceFrom.Value);

            if (filter.PriceTo.HasValue)
                query = query.Where(x => x.Price <= filter.PriceTo.Value);

            // TODO: add pages support

            return await query
                .ProjectTo<ProductDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }
    }
}
