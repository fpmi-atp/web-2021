﻿using AutoMapper;
using eShop.Domain;
using eShop.Usecases.Interfaces.Products.Commands.CreateProduct;
using eShop.Usecases.Interfaces.Products.Commands.UpdateProduct;
using eShop.Usecases.Interfaces.Products.Dto;
using eShop.Usecases.Interfaces.Products.Queries.SearchProducts;

namespace eShop.Usecases.Products.Mapping
{
    internal class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<CreateProductDto, Product>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Price, opt => opt.Ignore())
                .ForMember(x => x.PictureUri, opt => opt.Ignore());

            CreateMap<Product, ProductDetailDto>()
                .ForMember(x => x.AdditionalInfo, opt => opt.Ignore());

            CreateMap<UpdateProductDto, Product>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.CategoryId, opt => opt.Ignore())
                .ForMember(x => x.PictureUri, opt => opt.Ignore());

            CreateMap<Product, ProductDto>();
        }
    }
}
