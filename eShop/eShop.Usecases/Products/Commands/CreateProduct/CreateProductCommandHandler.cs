﻿using AutoMapper;
using eShop.DataAcccess.Interfaces;
using eShop.Domain;
using eShop.Usecases.Interfaces.Products.Commands.CreateProduct;
using eShop.Usecases.Interfaces.Products.Dto;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace eShop.Usecases.Products.Commands.CreateProduct
{
    internal class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, ProductDetailDto>
    {
        private readonly IDbContext _context;
        private readonly IMapper _mapper;

        public CreateProductCommandHandler(IDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProductDetailDto> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var product = _mapper.Map<Product>(request.Dto);

            _context.Products.Add(product);
            await _context.CommitAsync(cancellationToken);

            return _mapper.Map<ProductDetailDto>(product);
        }
    }
}
