﻿using eShop.DataAcccess.Interfaces;
using eShop.Usecases.Interfaces.Products.Commands.DeleteProduct;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace eShop.Usecases.Products.Commands.DeleteProduct
{
    internal class DeleteProductCommandHandler : AsyncRequestHandler<DeleteProductCommand>
    {
        private readonly IDbContext _context;

        public DeleteProductCommandHandler(IDbContext context)
        {
            _context = context;
        }

        protected override async Task Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            var product = await _context.Products.FindAsync(new object[] { request.Id }, cancellationToken);

            if (product == null)
                return;

            _context.Products.Remove(product);
            await _context.CommitAsync(cancellationToken);
        }
    }
}
