﻿using AutoMapper;
using eShop.DataAcccess.Interfaces;
using eShop.Usecases.Interfaces.Exceptions;
using eShop.Usecases.Interfaces.Products.Commands.UpdateProduct;
using eShop.Usecases.Interfaces.Products.Dto;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace eShop.Usecases.Products.Commands.UpdateProduct
{
    internal class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, ProductDetailDto>
    {
        private readonly IDbContext _context;
        private readonly IMapper _mapper;

        public UpdateProductCommandHandler(IDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ProductDetailDto> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            var product = await _context.Products.FindAsync(new object[] { request.Id }, cancellationToken);

            if (product == null)
            {
                throw new ObjectNotFoundException();
            }

            _mapper.Map(request.Dto, product);

            await _context.CommitAsync(cancellationToken);

            return _mapper.Map<ProductDetailDto>(product);
        }
    }
}
