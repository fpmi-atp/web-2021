﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace eShop.Usecases
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddUsecases(this IServiceCollection services)
        {
            // MediatR
            services.AddMediatR(Assembly.GetExecutingAssembly());

            // AutoMapper
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            return services;
        }
    }
}
