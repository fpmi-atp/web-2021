using AutoMapper;
using Xunit;

namespace eShop.Tests
{
    public class AutomapperTests
    {
        [Fact]
        public void Test_Configuration_Is_Valid()
        {
            // Arrange
            var config = new MapperConfiguration(cfg => {
                cfg.AddMaps("eShop.Usecases");
            });

            // Assert
            config.AssertConfigurationIsValid();

        }
    }
}
