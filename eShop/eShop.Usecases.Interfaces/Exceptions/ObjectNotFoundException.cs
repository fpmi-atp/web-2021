﻿using System;

namespace eShop.Usecases.Interfaces.Exceptions
{
    public class ObjectNotFoundException : Exception
    {
        public ObjectNotFoundException() : base("Object not found")
        {
        }
    }
}
