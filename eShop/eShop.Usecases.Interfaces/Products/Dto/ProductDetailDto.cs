﻿using System.Collections.Generic;

namespace eShop.Usecases.Interfaces.Products.Dto
{
    public class ProductDetailDto : ProductDtoBase
    {
        public Dictionary<string, string> AdditionalInfo { get; init; } = new Dictionary<string, string>();
    }
}
