﻿using System.ComponentModel.DataAnnotations;

namespace eShop.Usecases.Interfaces.Products.Dto
{
    public abstract class ProductDtoBase
    {
        public int Id { get; init; }

        [Required]
        [MaxLength(250)]
        public string Name { get; init; }

        public string Description { get; init; }

        public int CategoryId { get; init; }

        public decimal Price { get; init; }

        [MaxLength(250)]
        public string PictureUri { get; init; }
    }
}
