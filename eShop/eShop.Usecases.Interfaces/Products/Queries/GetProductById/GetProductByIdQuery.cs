﻿using eShop.Usecases.Interfaces.Products.Dto;
using MediatR;

namespace eShop.Usecases.Interfaces.Products.Queries.GetById
{
    public class GetProductByIdQuery : IRequest<ProductDetailDto>
    {
        public int ProductId { get; init; }
    }
}
