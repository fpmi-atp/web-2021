﻿using eShop.Usecases.Interfaces.Products.Dto;

namespace eShop.Usecases.Interfaces.Products.Queries.SearchProducts
{
    /// <summary>
    /// Product information
    /// </summary>
    public class ProductDto : ProductDtoBase
    {
    }
}
