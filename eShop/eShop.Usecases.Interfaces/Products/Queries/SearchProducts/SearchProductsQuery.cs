﻿using MediatR;
using System.Collections.Generic;

namespace eShop.Usecases.Interfaces.Products.Queries.SearchProducts
{
    public class SearchProductsQuery : IRequest<IReadOnlyList<ProductDto>>
    {
        public SearchProductsFilter Filter { get; init; }

        public int Page { get; init; }

        public int Limit { get; init; }
    }
}
