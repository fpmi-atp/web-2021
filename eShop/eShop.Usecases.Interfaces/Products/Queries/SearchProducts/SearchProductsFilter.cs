﻿namespace eShop.Usecases.Interfaces.Products.Queries.SearchProducts
{
    /// <summary>
    /// Filter
    /// </summary>
    public class SearchProductsFilter
    {
        /// <summary>
        /// Product name
        /// </summary>
        public string Name { get; init; }

        public int? CategoryId { get; init; }

        public decimal? PriceFrom { get; init; }

        public decimal? PriceTo { get; init; }
    }
}
