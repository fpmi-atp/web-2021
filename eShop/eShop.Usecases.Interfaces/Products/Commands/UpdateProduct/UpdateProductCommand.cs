﻿using eShop.Usecases.Interfaces.Products.Dto;
using MediatR;

namespace eShop.Usecases.Interfaces.Products.Commands.UpdateProduct
{
    public class UpdateProductCommand : IRequest<ProductDetailDto>
    {
        public int Id { get; init; }

        public UpdateProductDto Dto { get; init; }
    }
}
