﻿using System.ComponentModel.DataAnnotations;

namespace eShop.Usecases.Interfaces.Products.Commands.UpdateProduct
{
    public class UpdateProductDto
    {
        [Required]
        [MaxLength(250)]
        public string Name { get; init; }

        public string Description { get; init; }

        public decimal Price { get; init; }
    }
}
