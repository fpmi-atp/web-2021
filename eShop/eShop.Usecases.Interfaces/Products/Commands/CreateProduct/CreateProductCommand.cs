﻿using eShop.Usecases.Interfaces.Products.Dto;
using MediatR;

namespace eShop.Usecases.Interfaces.Products.Commands.CreateProduct
{
    public class CreateProductCommand : IRequest<ProductDetailDto>
    {
        public CreateProductDto Dto { get; init; }
    }
}
