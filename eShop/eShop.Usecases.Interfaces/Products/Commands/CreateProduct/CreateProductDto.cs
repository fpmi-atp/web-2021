﻿using System.ComponentModel.DataAnnotations;

namespace eShop.Usecases.Interfaces.Products.Commands.CreateProduct
{
    /// <summary>
    /// Create product data
    /// </summary>
    public class CreateProductDto
    {
        [Required]
        [MaxLength(250)]
        public string Name { get; init; }

        public string Description { get; init; }

        /// <summary>
        /// Readonly after creation
        /// </summary>
        public int CategoryId { get; init; }
    }
}
