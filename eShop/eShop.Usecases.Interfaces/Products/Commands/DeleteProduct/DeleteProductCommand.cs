﻿using MediatR;

namespace eShop.Usecases.Interfaces.Products.Commands.DeleteProduct
{
    public class DeleteProductCommand : IRequest
    {
        public int Id { get; init; }
    }
}
