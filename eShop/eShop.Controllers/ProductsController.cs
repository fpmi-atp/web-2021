﻿using eShop.Usecases.Interfaces.Products.Commands.CreateProduct;
using eShop.Usecases.Interfaces.Products.Commands.DeleteProduct;
using eShop.Usecases.Interfaces.Products.Commands.UpdateProduct;
using eShop.Usecases.Interfaces.Products.Dto;
using eShop.Usecases.Interfaces.Products.Queries.GetById;
using eShop.Usecases.Interfaces.Products.Queries.SearchProducts;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace eShop.Controllers
{
    /// <summary>
    /// Products
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly ISender _sender;

        public ProductsController(ISender sender)
        {
            _sender = sender;
        }

        /// <summary>
        /// Search products
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IReadOnlyList<ProductDto>> Search([FromQuery]SearchProductsFilter filter, 
            int page, int limit, CancellationToken cancellationToken)
        {
            // TODO: limit max value is 100.
            var query = new SearchProductsQuery { Filter = filter, Page = page, Limit = limit };
            return await _sender.Send(query, cancellationToken);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ProductDetailDto> GetById(int id, CancellationToken cancellationToken)
        {
            return await _sender.Send(new GetProductByIdQuery { ProductId = id }, cancellationToken);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ProductDetailDto> Create(CreateProductDto dto, CancellationToken cancellationToken)
        {
            return await _sender.Send(new CreateProductCommand { Dto = dto }, cancellationToken);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ProductDetailDto> Update(int id, UpdateProductDto dto, CancellationToken cancellationToken)
        {
            return await _sender.Send(new UpdateProductCommand { Id = id, Dto = dto }, cancellationToken);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task Delete(int id, CancellationToken cancellationToken)
        {
            await _sender.Send(new DeleteProductCommand { Id = id }, cancellationToken);
        }

        [HttpPost("{id}/uploadPicture")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public Task UploadPicture(int id, IFormFile file, CancellationToken cancellationToken)
        {
            // TODO: implement
            return Task.CompletedTask;
        }

        [HttpPost("{id}/deletePicture")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public Task DeletePicture(int id, CancellationToken cancellationToken)
        {
            // TODO: implement
            return Task.CompletedTask;
        }

    }
}
