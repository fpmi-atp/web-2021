﻿using System.ComponentModel.DataAnnotations;

namespace eShop.Domain
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }

        public decimal Price { get; set; }

        [MaxLength(250)]
        public string PictureUri { get; set; }
    }
}
