﻿using eShop.DataAcccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace eShop.DataAccess
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddEShopDataAccessSqlite(this IServiceCollection services, string connectionString)
        {
            services
                .AddDbContext<IDbContext, EShopDbContext>(
                    options => options
                        .UseSqlite(connectionString));

            return services;
        }
    }
}
