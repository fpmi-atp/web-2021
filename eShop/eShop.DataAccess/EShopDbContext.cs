﻿using eShop.DataAcccess.Interfaces;
using eShop.Domain;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace eShop.DataAccess
{
    internal class EShopDbContext : DbContext, IDbContext
    {
        public EShopDbContext(DbContextOptions<EShopDbContext> options) : base(options)
        {

        }

        public DbSet<Product> Products { get; set; }

        public Task<int> CommitAsync(CancellationToken cancellationToken) => SaveChangesAsync(cancellationToken);
    }
}
