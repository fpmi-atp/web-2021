const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Introduction to Web Development',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  dest: "public",
  base: "/mipt-web-course-2021/",

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
      {
        text: 'Lectures',
        link: '/lectures/',
      },
      {
        text: 'Syllabus',
        link: '/syllabus/'
      },
      {
        text: 'Code',
        link: 'https://gitlab.com/grandrust/mipt-web-course-2021/-/tree/main/code'
      }
    ],
    sidebar: {
      '/lectures/': [
        {
          title: 'Lecture 1',
          collapsable: true,
          children: [
            'lecture1/html',
            'lecture1/css',
            'lecture1/javascript',
            'lecture1/pet-project'
          ]
        },
        {
          title: 'Lecture 2',
          collapsable: true,
          children: [
            'lecture2/nodejs',
            'lecture2/npm',
            'lecture2/pet-project'
          ]
        },
        {
          title: 'Lecture 3',
          collapsable: true,
          children: [
            'lecture3/',
            'lecture3/webframework',
            'lecture3/typescript',
            'lecture3/webpack',
            'lecture3/pet-project'
          ]
        },
        {
          title: 'Lecture 4',
          collapsable: true,
          children: [
            'lecture4/reactjs',
            'lecture4/reduxjs',
            'lecture4/pet-project'
          ]
        },
        {
          title: 'Lecture 5',
          collapsable: true,
          children: [
            'lecture5/websocket',
            'lecture5/pet-project'
          ]
        }
      ]
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
