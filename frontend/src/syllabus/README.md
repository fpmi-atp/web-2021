# Syllabus

1. 03.11.2021 JavaScript Basics
2. 10.11.2021 NodeJs & NPM
3. 17.11.2021 JS frameworks review
4. 24.11.2021 ReactJs Essentials
5. 01.12.2021 Websocket

[https://jsfiddle.net/](https://jsfiddle.net/)
