# Pet Project

Refactor [task 3](../lecture3/pet-project.html) using react and redux.

1. Adjust typescript and webpack configurations to use react and redux
2. Rewrite coffee shop wit react and redux


See more details [here](https://gitlab.com/grandrust/mipt-web-course-2021/-/blob/main/code/lecture4/README.md)