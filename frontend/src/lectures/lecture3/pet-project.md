# Pet Project

Create simple webapp to host html from the [task 1](../lecture1/pet-project.html).

1. Create backend using koa/nestjs/another webframework
2. Rewrite task1 code with typescript
3. Use webpack to create a js bundle:
    1. add `ts-loader` to transpile typescript to javascript
    2. add html plugin to insert `<script>` tag with the bundle as a source
4. Write 2 npm scripts:
    1. `npm run build` - to build frontend
    2. `npm run start` - to run web app


See more details [here](https://gitlab.com/grandrust/mipt-web-course-2021/-/blob/main/code/lecture3/README.md)