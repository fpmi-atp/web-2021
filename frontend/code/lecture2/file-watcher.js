const fs = require("fs");
const dir = "./";

const fileStore = {};

setInterval(() => {
    fs.readdir(dir, (err, files) => { 
        files.forEach(file => {
          if (!fileStore[file])
          {
              console.log("Нашли новый файл: '" + file + "'");
              fileStore[file] = true;
          }
        });          
      });
}, 1000);

